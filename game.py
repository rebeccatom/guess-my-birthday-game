from random import randint

name = input("Hi! What is your name?: ")


for guess_number in range(1,6):
    random_month = randint(1, 12)
    random_year = randint(1924, 2004)
    print("Guess", guess_number, ":", name, ", were you born in ", random_month,"/",random_year)
    response = (input("Y/N?: "))

    if response == "Y":
        print("I knew it!")
        exit()
    elif guess_number == 5:
         print("I have other things to do.")
    else:
        print("Drat! Lemme try again!")
